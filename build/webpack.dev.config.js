//开发环境
var webpack = require('webpack')
var config = require('./webpack.base.config')
// dev配置
config.devServer= {
    hot: true,
    inline: true,
    proxy: {
      "/": {
          target: 'http://127.0.0.1:1235/',
          secure: false
      },
      "http://127.0.0.1:4000/": {
          target: 'http://test.venus.morning-star.cn/',
          secure: false
      }
    }
}
config.devtool = '#source-map'
module.exports = config