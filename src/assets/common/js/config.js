﻿ /*---------------------------------设置domain localStorage--------------------------------------------*/
/** eslint-disable **/
var domain = system();
/*-------------------------------------------后台配置------------------------------------------------*/ 
import systemList from './systemList'

module.exports={
	//登陆页面
    loginUrl: "http://127.0.0.1:4000/" + "html/login.html",

	//登陆成功后需要跳转到的页面                                                       
	homeUrl: "/index.html",  

	//请求头部信息地址
    loginApi: "http://127.0.0.1:4000/",  

	//根接口
    baseApi:"/",
	// baseApi:'/nodeserver/',

	//ajax 请求超时时间
	ajaxtimeout:1000000,

	//发送验证码时间间隔
	msgTime:60,
	                            
 	//隐藏显示时间
 	containerShowTime:10,

 	//pagesize 分页数量
 	pageSize:20,

 	systemList:systemList

};


function system() {
    if (localStorage.getItem('domain')) return localStorage.getItem('domain');
    var system = '';
    var url = location.hostname;
    var arr = url.split('.');
    var head = arr[0];
    if (head.indexOf('test') > -1 || head.indexOf('zane') > -1) {
        system = '//test.venus.' + arr[arr.length - 2] + '.' + arr[arr.length - 1] + '/'
    } else if (head.indexOf('dev') > -1) {
        var domain = arr[arr.length - 2] + '.' + arr[arr.length - 1];
        if (domain == 'morning-star.cn') {
            system = 'https://dev-venus.' + domain + '/'
        } else {
            system = '//dev-venus.' + domain + '/'
        };
    } else {
        var domain = arr[arr.length - 2] + '.' + arr[arr.length - 1];
        if (domain == 'morning-star.cn') {
            system = 'https://venus.' + domain + '/'
        } else {
            system = '//venus.' + domain + '/'
        };
    }
    localStorage.setItem('domain', system)
    return system
}