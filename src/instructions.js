
//自定义指令 

function instructions(Vue){
	let jurisdnArrs = util.getStorage('session','jurisdnArrs')?JSON.parse(util.getStorage('session','jurisdnArrs')):[]
	// 注册一个全局自定义指令 v-focus
	Vue.directive('Jurisdiction', {
	  	// 当绑定元素插入到 DOM 中。
	  	inserted: function (el,value) {

		  	let result = jurisdnArrs.includes(value.value)
		  	if(!result){
		  		$(el).remove();
		  	};

	  	},

	})
};




module.exports = instructions