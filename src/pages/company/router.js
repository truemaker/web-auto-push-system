// 模块路由设置
module.exports = [{
	path: '/company/companyList',
	name: 'companyList',
	meta: {
		title: '公司列表',
	},
	component: resolve => require(['./companyList.vue'], resolve)
},  ]