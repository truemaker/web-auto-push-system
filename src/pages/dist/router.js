// 模块路由设置
module.exports = [{
	path: '/dist/distHome',
	name: 'distHome',
	meta: {
		title: '集成环境系统发布',
	},
	component: resolve => require(['./distHome.vue'], resolve)
}, {
	path: '/dist/distAcHome',
	name: 'distAcHome',
	meta: {
		title: '集成环境活动发布',
	},
	component: resolve => require(['./distAcHome.vue'], resolve)
},{
	path: '/dist/distAnother',
	name: 'distAnother',
	meta: {
		title: '集成环境其他杂项发布',
	},
	component: resolve => require(['./distAnother.vue'], resolve)
},{
	path: '/dist/distPushLogs',
	name: 'distPushLogs',
	meta: {
		title: '集成环境发布日志',
	},
	component: resolve => require(['./distPushLogs.vue'], resolve)
},

]