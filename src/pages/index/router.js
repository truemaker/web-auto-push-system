// 模块路由设置
module.exports = [{
	path: '/index/index',
	name: 'index',
	meta: {
		title: '前端工具入口',
	},
	component: resolve => require(['./index.vue'], resolve)
}, ]