// 模块路由设置
module.exports = [{
	path: '/online/onlineHome',
	name: 'onlineHome',
	meta: {
		title: '正式环境系统发布',
	},
	component: resolve => require(['./onlineHome.vue'], resolve)
},{
	path: '/online/onlineAcHome',
	name: 'onlineAcHome',
	meta: {
		title: '正式环境活动发布',
	},
	component: resolve => require(['./onlineAcHome.vue'], resolve)
},{
	path: '/online/onlinePushLogs',
	name: 'onlinePushLogs',
	meta: {
		title: '正式服系统发布日志',
	},
	component: resolve => require(['./onlinePushLogs.vue'], resolve)
},{
	path: '/online/onlineAnother',
	name: 'onlineAnother',
	meta: {
		title: '正式服系统其他杂项发布',
	},
	component: resolve => require(['./onlineAnother.vue'], resolve)
},
]