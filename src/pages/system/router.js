// 模块路由设置
module.exports = [{
	path: '/system/systemList',
	name: 'systemList',
	meta: {
		title: '系统列表',
	},
	component: resolve => require(['./systemList.vue'], resolve)
},{
	path: '/system/emailManage',
	name: 'emailManage',
	meta: {
		title: '邮件管理',
	},
	component: resolve => require(['./emailManage.vue'], resolve)
},]