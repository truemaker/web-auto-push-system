//router.js 路由集合
import common from 'common/js/common'

//默认首页路由
const route = [{
	path: '/',
	name: 'home',
	meta: {
		title: '前端工具入口',
	},
	component: resolve => require(['commonvue/index.vue'], resolve)
}, ]


//合并路由
var routes = route.concat(
	require('pages/index/router'),
	require('pages/dist/router'),
	require('pages/online/router'),
	require('pages/company/router'),
	require('pages/system/router'),
)

module.exports = routes;